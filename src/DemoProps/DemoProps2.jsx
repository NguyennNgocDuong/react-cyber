import React, { Component } from "react";
import UserInfor from "./UserInfor";

export default class DemoProps extends Component {
  state = {
    name: "alice",
    age: 2,
  };

  handleChangName = () => {
    this.setState({
      name: "Bob",
    });
  };

  render() {
    return (
      <div>
        <p>DemoProps</p>
        <button
          onClick={() => this.handleChangName()}
          className="btn btn-primary"
        >
          Change name Bob
        </button>
        <UserInfor
          handleClick={this.handleChangName}
          dataUser={this.state}
          isLogin={true}
          title={`Hello ${this.state.name}`}
        />
      </div>
    );
  }
}
