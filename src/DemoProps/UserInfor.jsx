import React, { Component } from "react";

export default class UserInfor extends Component {
  render() {
    console.log(this.props);

    return (
      <div className="bg-warning">
        <p>UserInfor</p>
        <p>Name: {this.props.dataUser.name}</p>
        <p>Title: {this.props.title}</p>
        <button onClick={this.props.handleClick} className="btn btn-success">
          Change name to Bob
        </button>
      </div>
    );
  }
}
