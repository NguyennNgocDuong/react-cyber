import React, { Component } from "react";

export default class DataBinding extends Component {
  userAge = 2;

  renderFooter = () => {
    return <h1>Footer</h1>;
  };

  render() {
    let username = "Alice";
    let imgSrc =
      "https://cdn.popsww.com/blog/sites/2/2022/02/naruto-co-bao-nhieu-tap.jpg";
    return (
      <div>
        <div>Hello {username}</div>
        <p>User age: {this.userAge}</p>
        <img style={{ width: 500 }} src={imgSrc} alt="" />
        {this.renderFooter()}
      </div>
    );
  }
}
