import React, { Component } from "react";

export default class ItemShoes extends Component {
  render() {
    let { image, name } = this.props.shoes;

    return (
      <div className="col-3 my-3">
        <div className="card" style={{ width: "100%", height: "100%" }}>
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <a
              onClick={() => this.props.handleAddToCart(this.props.shoes)}
              href="#"
              className="btn btn-dark"
            >
              Add to cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}
