import React, { Component } from "react";
import Cart from "./Cart";
import { data_shoes } from "./dataShoes";
import ItemShoes from "./ItemShoes";

export default class ExShoes extends Component {
  state = {
    shoes: data_shoes,
    cart: [],
  };

  renderContent = () => {
    return this.state.shoes.map((item, index) => {
      return (
        <ItemShoes
          handleAddToCart={this.handleAddToCart}
          shoes={item}
          key={index}
        />
      );
    });
  };

  handleAddToCart = (shoes) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    console.log("index: ", index);

    let newCart = [...this.state.cart];
    // TH1: trong giỏ hàng chưa có sản phẩm
    if (index == -1) {
      let newSp = { ...shoes, soLuong: 1 };
      newCart.push(newSp);
    } else {
      newCart[index].soLuong++;
    }
    this.setState({
      cart: newCart,
    });

    // TH2: trong giỏ hàng đã có sản phẩm

    // let newCart = [...this.state.cart, shoes];
    // // this.setState là bất đồng bộ
    // this.setState({
    //   cart: newCart,
    // });
    // console.log("out render", this.state.cart.length);
  };

  render() {
    console.log("in render", this.state.cart.length);

    return (
      <div className="container py-5">
        <Cart cart={this.state.cart} />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
