import React, { Component } from "react";

export default class DemoState extends Component {
  // Nơi luuaw trữ dữ liệu lq đến component ~
  //   thay đổi dữ liệu thì cần tạo giao diện mới tương ứng
  state = {
    like: 0,
    userInfo: {
      name: "Alice",
    },
  };

  handleLike = () => {
    this.setState({
      // update giá trị của state
      like: this.state.like + 1,
    });
  };

  handleChangeName = () => {
    this.setState({
      userInfo: {
        name: "Bob",
      },
    });
  };

  // state thay đổi dẫn theo  render chạy lại
  render() {
    return (
      <div className="text-center py-5">
        <p>{this.state.like}</p>
        <button onClick={() => this.handleLike()} className="btn btn-primary">
          Like
        </button>
        <br />
        <p>Username: {this.state.userInfo.name}</p>
        <button
          onClick={() => this.handleChangeName()}
          className="btn btn-success"
        >
          Đổi tên
        </button>
      </div>
    );
  }
}
