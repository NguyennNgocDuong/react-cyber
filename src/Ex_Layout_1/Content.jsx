import React, { Component } from "react";

export default class Content extends Component {
  render() {
    return (
      <div className="bg-success text-light text-center py-5">
        Content Component
      </div>
    );
  }
}
