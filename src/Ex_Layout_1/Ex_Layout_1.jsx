import React, { Component } from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Home from "./Home";
import Navigate from "./Navigate";

export default class Ex_Layout_1 extends Component {
  render() {
    return (
      <>
        <Home />
        <Header />
        <div className="row">
          <div className="col-4">
            <Navigate />
          </div>
          <div className="col-8">
            <Content />
          </div>
        </div>
        <Footer />
      </>
    );
  }
}
