import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  isLogin = false;

  handleLogin = () => {
    this.isLogin = true;
  };

  renderContent = () => {
    if (this.isLogin) {
      return (
        <>
          <p>Bạn đã đăng nhập thành công</p>
          <button className="btn btn-danger">Đăng xuất</button>
        </>
      );
    }
    return (
      <>
        <p>Bạn chưa đăng nhập</p>
        <button onClick={() => this.handleLogin()} className="btn btn-success">
          Đăng nhập
        </button>
      </>
    );
  };

  render() {
    return (
      <div>
        {this.renderContent()}
        {/* {this.isLogin ? "Đã đăng nhập" : "Chưa đăng nhập"} */}
      </div>
    );
  }
}
