import logo from "./logo.svg";
import "./App.css";
import BaiTapComponent from "./components/BaiTap/BaiTapComponent";
import Student from "./components/DataBinding/Student";
import EventComponent from "./components/DataBinding/EventComponent";
import Demo_If from "./components/CauTrucDieuKhien_Rerender/Demo_If";
import BaiTapState from "./components/CauTrucDieuKhien_Rerender/BaiTapState";
import DemoVongLap from "./components/CauTrucVongLap/DemoVongLap";
import BaiTapVongLap from "./components/CauTrucVongLap/BaiTapVongLap";
import DemoProps from "./components/Props/DemoProps";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import Ex_Layout_1 from "./Ex_Layout_1/Ex_Layout_1";
import Ex_Layout_2 from "./Ex_Layout_2/Ex_Layout_2";
import DataBinding from "./DataBinding/DataBinding";
import EventBinding from "./EventBinding/EventBinding";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import DemoState from "./State/DemoState";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoProps2 from "./DemoProps/DemoProps2";
import ExShoes from "./Ex-Shoes/ExShoes";
import BaiTapTruyenFunc from "./components/Props/BaiTapTruyenFunc/BaiTapTruyenFunc";

function App() {
  return <div className="App">
    {/* <BaiTapComponent /> */}
    {/* <Student /> */}
    {/* <EventComponent /> */}
    {/* <Demo_If /> */}
    {/* <BaiTapState /> */}
    {/* <DemoVongLap /> */}
    {/* <BaiTapVongLap /> */}
    {/* <DemoProps /> */}
    {/* <BaiTapTruyenFunc /> */}


    {/* trên lớp */}
    {/* <DemoClass /> */}
    {/* <DemoFunction /> */}
    {/* <Ex_Layout_1 /> */}
    {/* <Ex_Layout_2 /> */}
    {/* <DataBinding /> */}
    {/* <EventBinding /> */}
    {/* <ConditionalRendering /> */}
    {/* <DemoState /> */}
    {/* <RenderWithMap /> */}
    {/* <DemoProps2 /> */}
    <ExShoes />


  </div>;
}

export default App;
