import React, { Component } from "react";

export default class EventBinding extends Component {
  // func khong tham số
  handleSayHello = () => {
    alert("Hello");
  };
  handleSayHelloByName = (name) => {
    alert("Helo " + name);
  };

  render() {
    let name = "Duong";
    return (
      <div className="text-center py-5">
        <button onClick={this.handleSayHello} className="btn btn-success">
          Say hello
        </button>
        <br />
        <button
          onClick={() => this.handleSayHelloByName(name)}
          className="btn btn-danger"
        >
          Say hello Alice
        </button>
      </div>
    );
  }
}
