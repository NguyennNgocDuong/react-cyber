export const phoneData = [
    {
        id: 1,
        name: "Adidas Prophere",
        price: 350,
        shortDescription:
            "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
        image: "http://svcy3.myclass.vn/images/adidas-prophere.png",
    },
    {
        id: 2,
        name: "Adidas Prophere Black White",
        price: 450,
        shortDescription:
            "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
        image: "http://svcy3.myclass.vn/images/adidas-prophere-black-white.png",
    },
    {
        id: 3,
        name: "Adidas Prophere Customize",
        price: 375,
        shortDescription:
            "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
        image: "http://svcy3.myclass.vn/images/adidas-prophere-customize.png",
    },

];