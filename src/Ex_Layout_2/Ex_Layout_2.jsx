import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import ListProduct from "./ListProduct";

export default class Ex_Layout_2 extends Component {
  render() {
    return (
      <>
        <Header />
        <Banner />
        <ListProduct />
        <Footer />
      </>
    );
  }
}
