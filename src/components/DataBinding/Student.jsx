import React, { Component } from "react";

export default class Student extends Component {
  // Thuộc tính
  hoTen = "Nguyễn Ngọc Dương";
  tuoi = "21";

  renderThongTinSV(truong) {
    return (
      <ul>
        <li>Họ tên: {this.hoTen}</li>
        <li>Tuổi: {this.tuoi}</li>
        <li>Trường: {truong}</li>
      </ul>
    );
  }
  //   Phương thức
  render() {
    const truong = "Đại học Văn Lang";

    return <div className="container">{this.renderThongTinSV(truong)}</div>;
  }
}
