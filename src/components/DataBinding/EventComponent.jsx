import React, { Component } from "react";

export default class EventComponent extends Component {
  handleShowMessage(message, name) {
    alert(`${message}${name}`);
  }

  render() {
    const message = "Hello ";
    const name = "Dương";

    return (
      <div className="container">
        {/* <button onClick={this.handleShowMessage.bind(this, message, name)}> */}
        <button onClick={() => this.handleShowMessage(message, name)}>
          Show message
        </button>
      </div>
    );
  }
}
