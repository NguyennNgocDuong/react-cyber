import React, { Component } from "react";

export default class Demo_If extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      userName: "",
    };
  }

  //   Phương thức render

  login = () => {
    this.setState({
      isLogin: true,
      userName: "Dương",
    });
  };

  logout = () => {
    this.setState({
      isLogin: false,
      userName: "",
    });
  };

  render() {
    return (
      <div>
        {/* {this.renderContent()} */}
        {this.state.isLogin == true ? (
          <div>
            <h2>Hello {this.state.userName}</h2>
            <button onClick={this.logout}>Logout</button>
          </div>
        ) : (
          <div>
            <button onClick={this.login}>Login</button>
          </div>
        )}
      </div>
    );
  }
}
