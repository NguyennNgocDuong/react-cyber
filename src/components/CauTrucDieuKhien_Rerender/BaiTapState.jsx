import React, { Component } from "react";

export default class BaiTapState extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img: "../img/products/black-car.jpg",
    };
  }

  changColor = (color) => {
    let imgSource = "";
    switch (color) {
      case "red": {
        imgSource = "../img/products/red-car.jpg";

        break;
      }
      case "black": {
        imgSource = "../img/products/black-car.jpg";
        break;
      }
      case "silver": {
        imgSource = "../img/products/silver-car.jpg";
      }
    }

    this.setState({
      img: imgSource,
    });
  };

  render() {
    return (
      <div>
        <div className="container py-5">
          <div className="row">
            <div className="col-6">
              <h2>Vui lòng chọn hình</h2>
              <img src={this.state.img} width={300} height={200} alt="" />
            </div>
            <div className="col-6">
              <button
                onClick={() => this.changColor("red")}
                className="btn btn-danger"
              >
                Red car
              </button>
              <button
                onClick={() => this.changColor("black")}
                className="btn btn-dark mx-5"
              >
                Black car
              </button>
              <button
                onClick={() => this.changColor("silver")}
                className="btn btn-light"
              >
                Silver car
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
