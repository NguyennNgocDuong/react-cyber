import React, { Component } from "react";
import data from "../../data/data.json";
import SanPham_RCC from "./SanPham_RCC";
import SanPham from "./SanPham_RCC.jsx";
import SanPham_RFC from "./SanPham_RFC.jsx";

export default class DemoProps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mangPhim: data,
    };
  }

  render() {
    return (
      <div className="container py-5">
        <div className="row">
          {this.state.mangPhim.map((phim, index) => {
            return (
              <div className="col-4" key={index}>
                {/* <SanPham_RFC phim={phim} /> */}
                <SanPham_RCC phim={phim} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
