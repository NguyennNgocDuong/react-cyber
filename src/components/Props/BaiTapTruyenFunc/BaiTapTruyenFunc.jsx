import React, { Component } from "react";
import DanhSachSanPham from "./DanhSachSanPham";
import { phoneData } from "../../../data/phoneData";

export default class BaiTapTruyenFunc extends Component {
  render() {
    return (
      <div>
        <DanhSachSanPham listProduct={phoneData} />
      </div>
    );
  }
}
