import React, { Component } from "react";

export default class DanhSachSanPham extends Component {
  state = {
    phoneData: this.props.listProduct,
  };
  renderProduct = () => {
    return this.state.phoneData.map((item, index) => {
      return (
        <div key={index} className="col-4">
          <div className="card ">
            <img className="card-img-top" src={item.image} alt />
            <div className="card-body">
              <h4 className="card-title">{item.name}</h4>
              <p className="card-text">{item.price}</p>
              <button className="btn btn-success">Xem chi tiết</button>
            </div>
          </div>
        </div>
      );
    });
  };

  render() {
    console.log(this.state.productDetail);
    return (
      <div className="container py-5">
        <div className="row">{this.renderProduct()}</div>
      </div>
    );
  }
}
