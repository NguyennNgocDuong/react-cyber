import React, { Component } from "react";

export default class Product extends Component {
  render() {
    let { product } = this.props;

    return (
      <div className="col-3">
        <div className="card text-white bg-dark">
          <img className="card-img-top" src={product.image} alt />
          <div className="card-body">
            <h4 className="card-title">{product.name}</h4>
            <p className="card-text">{product.shortDescription}</p>
          </div>
        </div>
      </div>
    );
  }
}
