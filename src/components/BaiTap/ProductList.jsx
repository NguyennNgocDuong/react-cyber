import React, { Component } from "react";
import Product from "./Product";

export default class ProductList extends Component {
  renderListProduct = () => {
    // lấy giá trị mảng sản phẩm từ component cha truyền vào thông qua thuộc tính this.props
    let { productList } = this.props;

    return productList.map((sp, index) => {
      return <Product product={sp} key={index} />;
    });
  };

  render() {
    return (
      <section className="productList" style={{ margin: "50px 0" }}>
        <h3 className="text-center">Best smartphone</h3>
        <div className="container">
          <div className="row">{this.renderListProduct()}</div>
        </div>
      </section>
    );
  }
}
