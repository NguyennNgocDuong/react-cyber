import React, { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";
import ProductList from "./ProductList";
import Slider from "./Slider";

export default class BaiTapComponent extends Component {
  listProduct = [
    {
      id: 1,
      name: "Adidas Prophere",
      price: 350,
      shortDescription:
        "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
      image: "http://svcy3.myclass.vn/images/adidas-prophere.png",
    },
    {
      id: 2,
      name: "Adidas Prophere Black White",
      price: 450,
      shortDescription:
        "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
      image: "http://svcy3.myclass.vn/images/adidas-prophere-black-white.png",
    },
    {
      id: 3,
      name: "Adidas Prophere Customize",
      price: 375,
      shortDescription:
        "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
      image: "http://svcy3.myclass.vn/images/adidas-prophere-customize.png",
    },
    {
      id: 4,
      name: "Adidas Super Star Red",
      price: 465,
      shortDescription:
        "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
      image: "http://svcy3.myclass.vn/images/adidas-super-star-red.png",
    },
  ];
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <ProductList productList={this.listProduct} />
        <Footer />
      </div>
    );
  }
}
