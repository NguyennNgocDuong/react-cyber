import React, { Component } from "react";

export default class DemoVongLap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mangSanPham: [
        { maSP: 1, tenSP: "IphoneX", gia: 1000 },
        { maSP: 2, tenSP: "Samsung", gia: 2000 },
        { maSP: 3, tenSP: "Oppo", gia: 3000 },
      ],
    };
  }
  //   Cách 1
  //   renderSanPham = () => {
  //     return this.state.mangSanPham.map((sp, index) => {
  //       return (
  //         <tr key={index}>
  //           <td>{sp.maSP}</td>
  //           <td>{sp.tenSP}</td>
  //           <td>{sp.gia}</td>
  //         </tr>
  //       );
  //     });
  //   };
  render() {
    return (
      <div className="container py-5">
        <table className="table bg-dark text-light py-5 text-center">
          <thead>
            <tr>
              <td>Mã sản phẩm</td>
              <td>Tên sản phẩm</td>
              <td>Giá sản phẩm</td>
            </tr>
          </thead>
          {/* <tbody>{this.renderSanPham()}</tbody> */}
          {/* Cách 2 */}
          <tbody>
            {this.state.mangSanPham.map((sp, index) => {
              return (
                <tr key={index}>
                  <td>{sp.maSP}</td>
                  <td>{sp.tenSP}</td>
                  <td>{sp.gia}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
