import React, { Component } from "react";
import data from "../../data/data.json";

export default class BaiTapVongLap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mangPhim: data,
    };
  }
  render() {
    return (
      <div>
        <h2 className="text-center">Danh sách phim</h2>
        <div className="container">
          <div className="row">
            {this.state.mangPhim.map((item, index) => {
              return (
                <div className="col-4 card " key={index}>
                  <img src={item.hinhAnh} className="card-img-top" alt="..." />
                  <div className="card-body">
                    <h2>{item.tenPhim}</h2>
                    <p className="card-text">
                      {item.moTa.length > 100
                        ? item.moTa.substr(0, 150) + "..."
                        : item.moTa}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
