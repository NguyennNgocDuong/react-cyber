import React, { Component } from "react";
import { data } from "./dataGlasses";
import styles from "./renderWithMap.module.css";

export default class RenderWithMap extends Component {
  state = {
    glasses: data,
  };

  handleChangeGlass = (data) => {
    console.log("data: ", data);
  };

  renderList = () => {
    return this.state.glasses.map((item, index) => {
      return (
        <div className="col-4" key={index}>
          <img
            onClick={() => this.handleChangeGlass(item)}
            style={{ width: 200, margin: "50px 0" }}
            src={item.url}
            alt=""
          />
          ;
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        <div className="row">{this.renderList()}</div>
        {/* <div className={styles.title}>Hellpo title</div> */}
        {/* <img src="./glassesImage/g1.jpg" alt="" /> */}
      </div>
    );
  }
}
